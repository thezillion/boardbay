var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var session = require('express-session');

var passport = require('passport');
var port = 3000;

var app = express();

//View Engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

// Set Static Folder
app.use(express.static(path.join(__dirname, 'client')));

// Body Parser MW
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(session({ secret: 'bookyzia_secret_21dsaj5g49rvc1vgy', resave: false, saveUninitialized: true, cookie: { secure: true } }));
app.use(passport.initialize());
app.use(passport.session());

var index = require('./routes/index');
var api = require('./routes/api');
var auth = require('./routes/auth');

app.use('/', index);
app.use('/api', api);
app.use('/auth', auth);

app.listen(port, function(){
    console.log('Server started on port '+port);
});

app.use(function(req, res, next){
  // respond with html page
  if (req.accepts('html')) {
    res.render('index.html');
    return;
  }
});
