var express = require('express');
var router = express.Router();
var firebase = require('../firebase/firebase.js');
var jwt = require('jsonwebtoken');
var jwt_secret = 'abcdef';

router.use(function(req, res, next) {
  var token = req.body.token;
  if (token) {
    jwt.verify(token, jwt_secret, function(err, decoded) {
      if (err) {
        res.json({ success: false, message: 'Invalid token' });
      } else {
        req.verifiedUser = true;
        req.user_data = decoded;
        next();
      }
    });
  } else {
    next();
  }
});

var gmaps = require('./api/gmaps.js')(firebase, jwt_secret);

router.get('/firebase-test/put/:val', function(req, res) {
  firebase.database().ref('users').set({
    value: req.params.val
  });
  res.send("done");
});

router.use('/gmaps', gmaps);

// var db = mongojs('mongodb://<dbuser>:<dbpassword>@ds127443.mlab.com:27443/contempt', ['ducks']);
//
// // Get All Tasks
// router.get('/tasks', function(req, res, next){
//     db.tasks.find(function(err, tasks){
//         if(err){
//             res.send(err);
//         }
//         res.json(tasks);
//     });
// });
//
// // Get Single Task
// router.get('/task/:id', function(req, res, next){
//     db.tasks.findOne({_id: mongojs.ObjectId(req.params.id)}, function(err, task){
//         if(err){
//             res.send(err);
//         }
//         res.json(task);
//     });
// });
//
// //Save Task
// router.post('/task', function(req, res, next){
//     var task = req.body;
//     if(!task.title || !(task.isDone + '')){
//         res.status(400);
//         res.json({
//             "error": "Bad Data"
//         });
//     } else {
//         db.tasks.save(task, function(err, task){
//             if(err){
//                 res.send(err);
//             }
//             res.json(task);
//         });
//     }
// });
//
// // Delete Task
// router.delete('/task/:id', function(req, res, next){
//     db.tasks.remove({_id: mongojs.ObjectId(req.params.id)}, function(err, task){
//         if(err){
//             res.send(err);
//         }
//         res.json(task);
//     });
// });
//
// // Update Task
// router.put('/task/:id', function(req, res, next){
//     var task = req.body;
//     var updTask = {};
//
//     if(task.isDone){
//         updTask.isDone = task.isDone;
//     }
//
//     if(task.title){
//         updTask.title = task.title;
//     }
//
//     if(!updTask){
//         res.status(400);
//         res.json({
//             "error":"Bad Data"
//         });
//     } else {
//         db.tasks.update({_id: mongojs.ObjectId(req.params.id)},updTask, {}, function(err, task){
//         if(err){
//             res.send(err);
//         }
//         res.json(task);
//     });
//     }
// });

module.exports = router;
