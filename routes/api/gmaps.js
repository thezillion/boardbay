var jwt = require('jsonwebtoken');
var express = require('express');
var router = express.Router();

var request = require('request');

function randomString() {
  return 'abcdefghijklmnopqrstuvwxyz123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('').sort(function(a, b) {return Math.random()>.5 ? -1 : 1;}).splice(4, 10).join('');
}

// do npm install '@google/maps'
function getTrafficScore(key,origins,destinations,callback){
  const gmaps = require('@google/maps').createClient({ key });

  gmaps.distanceMatrix({
    origins,
    destinations
  },(err,resp) => {
    if(!err){
      var sum = 0,count = 0;
      const data = resp.json.rows;

      for (let i=0;i<data.length;++i){
        if (data[i].elements[0].status !== '')
        for(let j=0;j<data[i].elements.length;++j){
          var item = data[i].elements[j];
          sum+= item.distance.value/item.duration.value;
          count++;
        }
      }

      var avg = sum/count;
      callback(avg);
    }
  });
}

module.exports = function(firebase, jwt_secret) {

  var GoogleMapsAPI = require('googlemaps');

  const publicConfig = {
    key: 'AIzaSyAlzPXzickU3d77PuMzRDftpgmpYsEC434',
    stagger_time:       1000, // for elevationPath
    encode_polylines:   false,
    secure:             true // use https
  };

  const gmAPI = new GoogleMapsAPI(publicConfig);

  router.post('/add-billboard', function(req, res) {

    if (req.verifiedUser && req.user_data) {

      var newSpace = req.body.newSpace;

      var place = newSpace.place;
      var price = newSpace.price;
      var size = newSpace.size;

      var user_data = req.user_data;
      var lat = newSpace.lat, lng = newSpace.lng;

      const data = {
        "location": [lat, lng].join(),
        "radius"  : "500",
        "rankby"  : "prominence"
      };

      var tags = {};
      let origins = new Set();

      gmAPI.placeSearch(data,(err,result) => {
        // console.log(result);
        result.results.forEach(function(element) {
          element.types.forEach((type) => {
            if (!tags[type]) tags[type] = 0;
            tags[type]++;
            origins.add([element.geometry.location.lat, element.geometry.location.lng].join());
          });
        }, this);

        let origins_array = Array.from(origins);
        var destinations = [[lat, lng].join()];

        getTrafficScore(publicConfig.key, origins_array, destinations, (avg) => {
          firebase.database().ref(['/users', user_data.id, randomString()].join('/')).set({place, lat, lng, size, tags, traffic: avg}, function(snapshot) {
            res.send("done");
          });
        });

      });

    }

  });

  function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1);
    var a =
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
      Math.sin(dLon/2) * Math.sin(dLon/2)
      ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c; // Distance in km
    return d;
  }

  function deg2rad(deg) {
    return deg * (Math.PI/180)
  }

  router.post('/lookup-billboard', function(req, res) {

    if (req.verifiedUser && req.user_data) {

      var user_data = req.user_data;
      var user_tags = req.body.tags;

      firebase.database().ref('/users/'+user_data.id).once('value', function(snapshot) {
        var records = snapshot.val();
        delete records["username"];
        var ids = Object.keys(records);
        var srcLatLng = req.body.srcLatLng;
        var result = [];
        var i = 0;
        ids.forEach((id) => {
          var record = records[id];
          record.lat = parseFloat(record.lat);
          record.lng = parseFloat(record.lng);
          var distance = getDistanceFromLatLonInKm(srcLatLng.lat, srcLatLng.lng, record.lat, record.lng);
          if (distance <= 15) {
            record.traffic = parseFloat(record.traffic);
            var req_body = { user: user_tags, tags: record.tags, key: id };
            request.post({
              headers: {'content-type' : 'application/json'},
              url:     'http://139.59.73.187/api/simscore',
              body:    JSON.stringify(req_body)
            }, function(error, response, body){
              var data = JSON.parse(body);
              var k = data.key;
              console.log(data);
              result.push({ title: records[k].place, lat: records[k].lat, lng: records[k].lng, data: data.data, traffic: records[k].traffic});
              i++;
              if (i == ids.length) {
                res.send({ success: true, message: result });
              }
            });
          } else {
            i++;
          }
        });
      });

    }

  });

  router.get('/get-place/:str', function(req, res) {
    request.get({
      headers: {'content-type' : 'application/json'},
      url: ['https://maps.googleapis.com/maps/api/place/textsearch/json?query=', req.params.str, '&key=', publicConfig.key].join(''),
    }, function(error, response, body){
      res.json(JSON.parse(body));
    });
  });

  router.post('/get-bb-locations', function(req, res) {
    if (req.verifiedUser && req.user_data) {
      var user_data = req.user_data;
      firebase.database().ref('/users/'+user_data.id).once('value', function(snapshot) {
        res.json({success: true, message: snapshot});
      });
    }
  });

  return router;

};
