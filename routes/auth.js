var express = require('express');
var router = express.Router();
var firebase = require('../firebase/firebase.js');
var jwt = require('jsonwebtoken');

var passport = require('passport')
  , FacebookStrategy = require('passport-facebook').Strategy;

var jwt_secret = 'abcdef';

passport.serializeUser(function(user, done) {
  token = jwt.sign({'id': user.id}, jwt_secret, { expiresIn: '24h' });
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  done(err, user);
});

passport.use(new FacebookStrategy({
    clientID: 1839172432774753,
    clientSecret: "55ba213b5cef4e836df6c39384a5de16",
    callbackURL: "http://localhost:3000/auth/facebook/callback",
    profileFields: ['id', 'first_name', 'middle_name', 'last_name', 'email']
  },
  function(accessToken, refreshToken, profile, done) {
    firebase.database().ref('/users').once('value', function(snapshot) {
      if (snapshot.hasChild(profile.id)) {
        // user exists
        done(null, profile);
      } else {
        // user doesnt exist
        firebase.database().ref('/users/'+profile.id).set({
          username: profile.id
        }, function(snapshot) {
          done(null, profile);
        });
      }
    });
  }
));

// Redirect the user to Facebook for authentication.  When complete,
// Facebook will redirect the user back to the application at
//     /auth/facebook/callback
router.get('/facebook', passport.authenticate('facebook', { scope: ['user_posts', 'email']}));

// Facebook will redirect the user to this URL after approval.  Finish the
// authentication process by attempting to obtain an access token.  If
// access was granted, the user will be logged in.  Otherwise,
// authentication has failed.
router.get('/facebook/callback', passport.authenticate('facebook', {  failureRedirect: '/login' }), function(req, res) {
  res.redirect('/callback/'+token);
});

module.exports = router;
