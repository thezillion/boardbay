"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var forms_1 = require("@angular/forms");
var core_2 = require("@agm/core");
var ng2_charts_1 = require("ng2-charts/ng2-charts");
var app_routing_module_1 = require("./app-routing.module");
var app_component_1 = require("./app.component");
var home_component_1 = require("./components/home/home.component");
var chicks_component_1 = require("./components/chicks/chicks.component");
var ducks_component_1 = require("./components/ducks/ducks.component");
var ducks_add_component_1 = require("./components/ducks/add/ducks-add.component");
var ducks_edit_component_1 = require("./components/ducks/edit/ducks-edit.component");
var callback_component_1 = require("./components/callback/callback.component");
var user_service_1 = require("./services/user.service");
var adspace_service_1 = require("./services/adspace.service");
var object_pipe_1 = require("./pipes/object.pipe");
var about_component_1 = require("./components/about/about.component");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule, http_1.HttpModule, forms_1.FormsModule, app_routing_module_1.AppRoutingModule,
                core_2.AgmCoreModule.forRoot({
                    apiKey: 'AIzaSyAlzPXzickU3d77PuMzRDftpgmpYsEC434'
                }),
                ng2_charts_1.ChartsModule
            ],
            declarations: [app_component_1.AppComponent, home_component_1.HomeComponent, chicks_component_1.ChicksComponent, ducks_component_1.DucksComponent, ducks_add_component_1.DucksAddComponent, ducks_edit_component_1.DucksEditComponent, about_component_1.AboutComponent, callback_component_1.CallbackComponent,
                object_pipe_1.KeysPipe,
                object_pipe_1.ValuesPipe,],
            bootstrap: [app_component_1.AppComponent],
            providers: [adspace_service_1.AdSpaceService, user_service_1.UserService]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map