import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms'
import { AgmCoreModule } from '@agm/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { ChicksComponent } from './components/chicks/chicks.component';
import { DucksComponent } from './components/ducks/ducks.component';
import { DucksAddComponent } from './components/ducks/add/ducks-add.component';
import { DucksEditComponent } from './components/ducks/edit/ducks-edit.component';
import { CallbackComponent } from './components/callback/callback.component';

import { UserService } from './services/user.service';
import { AdSpaceService } from './services/adspace.service';

import { KeysPipe, ValuesPipe } from './pipes/object.pipe';

import { AboutComponent } from './components/about/about.component';


@NgModule({
  imports:      [
    BrowserModule, HttpModule, FormsModule, AppRoutingModule,
    AgmCoreModule.forRoot({
        apiKey: 'AIzaSyAlzPXzickU3d77PuMzRDftpgmpYsEC434'
      }),
    ChartsModule
  ],
  declarations: [ AppComponent, HomeComponent, ChicksComponent, DucksComponent, DucksAddComponent, DucksEditComponent, AboutComponent, CallbackComponent,
    KeysPipe,
      ValuesPipe, ],
  bootstrap: [ AppComponent ],
  providers: [ AdSpaceService, UserService ]
})
export class AppModule { }
