import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { DucksComponent } from './components/ducks/ducks.component';
import { DucksAddComponent } from './components/ducks/add/ducks-add.component';
import { DucksEditComponent } from './components/ducks/edit/ducks-edit.component';
import { ChicksComponent } from './components/chicks/chicks.component';
import { AboutComponent } from './components/about/about.component';
import { CallbackComponent } from './components/callback/callback.component';

const routes: Routes = [
  { path: '',  component: HomeComponent },
  { path: 'agency', component: DucksComponent, children: [
    { path: '', redirectTo: 'add', pathMatch: 'full' },
    { path: 'add', component: DucksAddComponent },
    { path: 'edit', component: DucksEditComponent }
  ] },
  { path: 'business-owner',  component: ChicksComponent },
  { path: 'about', component: AboutComponent },
  { path: 'callback/:token', component: CallbackComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}
