import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

import { UserService } from './user.service';

@Injectable()
export class MapsService{

  apiKey: string = 'AIzaSyAlzPXzickU3d77PuMzRDftpgmpYsEC434';
  constructor(
    private http:Http,
    private userService: UserService
  ){
      console.log('AdSpace Service Initialized...');
  }

  makeAPICall(url, data) {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    if (!data) data = {};
    data["token"] = this.userService.getToken();
    return this.http.post(url, JSON.stringify(data), {headers: headers})
        .map(res => res.json());
  }

  addBillboard(kw){
    return this.http.get(['https://maps.googleapis.com/maps/api/place/autocomplete/json?input=', kw.replace(/ /g, '').toLowerCase(), '&types=geocode&key=', this.apiKey].join(''))
      .map(res => res.json());
  }
}
