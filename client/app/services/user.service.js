"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var UserService = (function () {
    function UserService(http) {
        this.http = http;
        console.log('User Service Initialized...');
    }
    UserService.prototype.isLoggedIn = function () {
        return this.getToken() ? true : false;
    };
    UserService.prototype.logout = function () {
        this.setToken(null);
    };
    UserService.prototype.setToken = function (token) {
        if (token)
            localStorage.setItem('currentUser', token);
        else
            localStorage.removeItem('currentUser');
    };
    UserService.prototype.getToken = function () {
        return localStorage.getItem('currentUser');
    };
    UserService.prototype.getUser = function () {
        // if (this.isLoggedIn()) {
        //   var data = { token: this.getToken() };
        //   var headers = new Headers();
        //   headers.append('Content-Type', 'application/json');
        //   var loader = new ProgressiveLoader();
        //   loader.placeLoader('UserGet');
        //   return this.http.post(getAPIRoot()+'/auth/me', JSON.stringify(data), { headers: headers })
        //           .map(res => {
        //             // If the token was tampered with, logout
        //             var x = res.json();
        //             loader.removeLoader();
        //             if (!x.success) window.location.href = "/auth/logout";
        //             else return x.message;
        //           });
        // }
    };
    UserService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http])
    ], UserService);
    return UserService;
}());
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map