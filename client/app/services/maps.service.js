"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var user_service_1 = require("./user.service");
var MapsService = (function () {
    function MapsService(http, userService) {
        this.http = http;
        this.userService = userService;
        this.apiKey = 'AIzaSyAlzPXzickU3d77PuMzRDftpgmpYsEC434';
        console.log('AdSpace Service Initialized...');
    }
    MapsService.prototype.makeAPICall = function (url, data) {
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        if (!data)
            data = {};
        data["token"] = this.userService.getToken();
        return this.http.post(url, JSON.stringify(data), { headers: headers })
            .map(function (res) { return res.json(); });
    };
    MapsService.prototype.addBillboard = function (kw) {
        return this.http.get(['https://maps.googleapis.com/maps/api/place/autocomplete/json?input=', kw.replace(/ /g, '').toLowerCase(), '&types=geocode&key=', this.apiKey].join(''))
            .map(function (res) { return res.json(); });
    };
    MapsService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http,
            user_service_1.UserService])
    ], MapsService);
    return MapsService;
}());
exports.MapsService = MapsService;
//# sourceMappingURL=maps.service.js.map