import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class UserService {
  constructor(
    private http: Http
  ){
    console.log('User Service Initialized...');
  }

  isLoggedIn() {
    return this.getToken() ? true : false;
  }

  logout() {
    this.setToken(null);
  }

  setToken(token) {
    if (token)
      localStorage.setItem('currentUser', token);
    else
      localStorage.removeItem('currentUser');
  }


  getToken(): string {
    return localStorage.getItem('currentUser');
  }

  getUser() {
    // if (this.isLoggedIn()) {
    //   var data = { token: this.getToken() };
    //   var headers = new Headers();
    //   headers.append('Content-Type', 'application/json');
    //   var loader = new ProgressiveLoader();
    //   loader.placeLoader('UserGet');
    //   return this.http.post(getAPIRoot()+'/auth/me', JSON.stringify(data), { headers: headers })
    //           .map(res => {
    //             // If the token was tampered with, logout
    //             var x = res.json();
    //             loader.removeLoader();
    //             if (!x.success) window.location.href = "/auth/logout";
    //             else return x.message;
    //           });
    // }
  }
}
