import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

import { UserService } from './user.service';

@Injectable()
export class AdSpaceService{

    apiKey: string = 'AIzaSyAlzPXzickU3d77PuMzRDftpgmpYsEC434';
    constructor(
      private http:Http,
      private userService: UserService
    ){
        console.log('AdSpace Service Initialized...');
    }

    makeAPICall(url, data) {
      var headers = new Headers();
      headers.append('Content-Type', 'application/json');
      if (!data) data = {};
      data["token"] = this.userService.getToken();
      return this.http.post(url, JSON.stringify(data), {headers: headers})
          .map(res => {
            var x = res.json();
            console.log(x);
            if (!x.success) window.location.href = "/auth/logout";
            else return x.message;
          });
    }

    addBillboard(newSpace){
      return this.makeAPICall('/api/gmaps/add-billboard', { newSpace });
    }

    getPlaces(tags, srcLatLng) {
      return this.makeAPICall('/api/gmaps/lookup-billboard', { tags, srcLatLng });
    }

    findPlaceCoord(str) {
      var headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.get('/api/gmaps/get-place/'+str, { headers: headers })
        .map(res => {
          return res.json();
        });
    }

    getAllBillboards() {
      return this.makeAPICall('/api/gmaps/get-bb-locations', {});
    }

    // getTasks(){
    //     return this.http.get('/api/tasks')
    //         .map(res => res.json());
    // }
    //
    // addTask(newTask){
    //     var headers = new Headers();
    //     headers.append('Content-Type', 'application/json');
    //     return this.http.post('/api/task', JSON.stringify(newTask), {headers: headers})
    //         .map(res => res.json());
    // }
    //
    // deleteTask(id){
    //     return this.http.delete('/api/task/'+id)
    //         .map(res => res.json());
    // }
    //
    // updateStatus(task){
    //     var headers = new Headers();
    //     headers.append('Content-Type', 'application/json');
    //     return this.http.put('/api/task/'+task._id, JSON.stringify(task), {headers: headers})
    //         .map(res => res.json());
    // }
}
