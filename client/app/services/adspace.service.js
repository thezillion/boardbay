"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var user_service_1 = require("./user.service");
var AdSpaceService = (function () {
    function AdSpaceService(http, userService) {
        this.http = http;
        this.userService = userService;
        this.apiKey = 'AIzaSyAlzPXzickU3d77PuMzRDftpgmpYsEC434';
        console.log('AdSpace Service Initialized...');
    }
    AdSpaceService.prototype.makeAPICall = function (url, data) {
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        if (!data)
            data = {};
        data["token"] = this.userService.getToken();
        return this.http.post(url, JSON.stringify(data), { headers: headers })
            .map(function (res) {
            var x = res.json();
            console.log(x);
            if (!x.success)
                window.location.href = "/auth/logout";
            else
                return x.message;
        });
    };
    AdSpaceService.prototype.addBillboard = function (newSpace) {
        return this.makeAPICall('/api/gmaps/add-billboard', { newSpace: newSpace });
    };
    AdSpaceService.prototype.getPlaces = function (tags, srcLatLng) {
        return this.makeAPICall('/api/gmaps/lookup-billboard', { tags: tags, srcLatLng: srcLatLng });
    };
    AdSpaceService.prototype.findPlaceCoord = function (str) {
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.get('/api/gmaps/get-place/' + str, { headers: headers })
            .map(function (res) {
            return res.json();
        });
    };
    AdSpaceService.prototype.getAllBillboards = function () {
        return this.makeAPICall('/api/gmaps/get-bb-locations', {});
    };
    AdSpaceService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http,
            user_service_1.UserService])
    ], AdSpaceService);
    return AdSpaceService;
}());
exports.AdSpaceService = AdSpaceService;
//# sourceMappingURL=adspace.service.js.map