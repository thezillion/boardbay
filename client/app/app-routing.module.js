"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var home_component_1 = require("./components/home/home.component");
var ducks_component_1 = require("./components/ducks/ducks.component");
var ducks_add_component_1 = require("./components/ducks/add/ducks-add.component");
var ducks_edit_component_1 = require("./components/ducks/edit/ducks-edit.component");
var chicks_component_1 = require("./components/chicks/chicks.component");
var about_component_1 = require("./components/about/about.component");
var callback_component_1 = require("./components/callback/callback.component");
var routes = [
    { path: '', component: home_component_1.HomeComponent },
    { path: 'agency', component: ducks_component_1.DucksComponent, children: [
            { path: '', redirectTo: 'add', pathMatch: 'full' },
            { path: 'add', component: ducks_add_component_1.DucksAddComponent },
            { path: 'edit', component: ducks_edit_component_1.DucksEditComponent }
        ] },
    { path: 'business-owner', component: chicks_component_1.ChicksComponent },
    { path: 'about', component: about_component_1.AboutComponent },
    { path: 'callback/:token', component: callback_component_1.CallbackComponent }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forRoot(routes)],
            exports: [router_1.RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=app-routing.module.js.map