import { Component, OnInit } from '@angular/core';

import { UserService } from '../../services/user.service';

import { ActivatedRoute, Params, Router } from '@angular/router';

import { Location } from '@angular/common';

@Component({
  moduleId: module.id,
  selector: 'callback',
  templateUrl: 'callback.component.html',
})

export class CallbackComponent {
    constructor(
      private userService: UserService,
      private route: ActivatedRoute,
      private router: Router
    ){

    }

    ngOnInit() {
      this.route.params
      .subscribe(params => {
        this.userService.setToken(params['token']);
        this.router.navigate(['/agency']);
      });
    }

}
