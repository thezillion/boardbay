import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';

import { AdSpaceService } from '../../services/adspace.service';
import { MapsService } from '../../services/maps.service';

@Component({
  moduleId: module.id,
  selector: 'chicks',
  templateUrl: 'chicks.component.html',
  styleUrls: ['chicks.component.css']
})

export class ChicksComponent implements OnInit {
    coords = [
      {
        lat: 9.9312,
        lng: 76.2673
      }

    ];

    pieData;
    trafficData;

    searchLocation: string;
    tags: any[];
    tagName: string;
    srcLocation;

    constructor(
      private adSpaceService: AdSpaceService,
      private http: Http
    ){}

    ngOnInit() {
      this.searchLocation = 'Kochi';
      this.tags = [];
      this.findPlaceCoord();
    }

    addTag(event) {
      event.preventDefault();
      if (this.tagName) {
        this.tags.push(this.tagName);
      }
      this.tagName = "";
    }

    deleteTag(event, tag) {
      event.preventDefault();
      this.tags.splice(this.tags.indexOf(tag), 1);
    }

    searchPlaces() {
      if (this.tags.length > 0 && this.srcLocation) {
        this.adSpaceService.getPlaces(this.tags, this.srcLocation.results[0].geometry.location)
          .subscribe(res => {
            this.coords = res;
            this.pieData = {};
            this.trafficData = {};
            console.log(res);
            res.forEach(c => {
              this.pieData[c.title] = c.data.finalScore;
              this.trafficData[c.title+" traffic"] = 1/c.traffic;
              console.log(c);
            });
          });
      }
    }

    findPlaceCoord() {
      this.adSpaceService.findPlaceCoord(this.searchLocation)
        .subscribe(res => {
          this.srcLocation = res;
          this.searchPlaces();
        });
    }
}
