"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var adspace_service_1 = require("../../services/adspace.service");
var ChicksComponent = (function () {
    function ChicksComponent(adSpaceService, http) {
        this.adSpaceService = adSpaceService;
        this.http = http;
        this.coords = [
            {
                lat: 9.9312,
                lng: 76.2673
            }
        ];
    }
    ChicksComponent.prototype.ngOnInit = function () {
        this.searchLocation = 'Kochi';
        this.tags = [];
        this.findPlaceCoord();
    };
    ChicksComponent.prototype.addTag = function (event) {
        event.preventDefault();
        if (this.tagName) {
            this.tags.push(this.tagName);
        }
        this.tagName = "";
    };
    ChicksComponent.prototype.deleteTag = function (event, tag) {
        event.preventDefault();
        this.tags.splice(this.tags.indexOf(tag), 1);
    };
    ChicksComponent.prototype.searchPlaces = function () {
        var _this = this;
        if (this.tags.length > 0 && this.srcLocation) {
            this.adSpaceService.getPlaces(this.tags, this.srcLocation.results[0].geometry.location)
                .subscribe(function (res) {
                _this.coords = res;
                _this.pieData = {};
                _this.trafficData = {};
                console.log(res);
                res.forEach(function (c) {
                    _this.pieData[c.title] = c.data.finalScore;
                    _this.trafficData[c.title + " traffic"] = 1 / c.traffic;
                    console.log(c);
                });
            });
        }
    };
    ChicksComponent.prototype.findPlaceCoord = function () {
        var _this = this;
        this.adSpaceService.findPlaceCoord(this.searchLocation)
            .subscribe(function (res) {
            _this.srcLocation = res;
            _this.searchPlaces();
        });
    };
    ChicksComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'chicks',
            templateUrl: 'chicks.component.html',
            styleUrls: ['chicks.component.css']
        }),
        __metadata("design:paramtypes", [adspace_service_1.AdSpaceService,
            http_1.Http])
    ], ChicksComponent);
    return ChicksComponent;
}());
exports.ChicksComponent = ChicksComponent;
//# sourceMappingURL=chicks.component.js.map