"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var adspace_service_1 = require("../../../services/adspace.service");
var DucksEditComponent = (function () {
    function DucksEditComponent(adSpaceService) {
        this.adSpaceService = adSpaceService;
    }
    DucksEditComponent.prototype.ngOnInit = function () {
        this.loadAllBBLocation();
        this.coords = [];
    };
    DucksEditComponent.prototype.loadAllBBLocation = function () {
        var _this = this;
        this.adSpaceService.getAllBillboards()
            .subscribe(function (loc) {
            var z = loc;
            delete z["username"];
            var x = Object.keys(z);
            x.forEach(function (y) {
                _this.coords.push({ lat: parseFloat(z[y].lat), lng: parseFloat(z[y].lng) });
            });
        });
    };
    DucksEditComponent.prototype.openEditWindow = function (o) {
        console.log(o);
    };
    DucksEditComponent.prototype.logpappu = function () {
        console.log("pappu");
    };
    DucksEditComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'ducks-edit',
            templateUrl: 'ducks-edit.component.html',
            styleUrls: ['ducks-edit.component.css']
        }),
        __metadata("design:paramtypes", [adspace_service_1.AdSpaceService])
    ], DucksEditComponent);
    return DucksEditComponent;
}());
exports.DucksEditComponent = DucksEditComponent;
var DucksEditWindowComponent = (function () {
    function DucksEditWindowComponent() {
    }
    DucksEditWindowComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'ducks-edit-window',
            template: "\n  <div class=\"modal fade\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n        <h4 class=\"modal-title\">Modal Header</h4>\n      </div>\n      <div class=\"modal-body\">\n        <p>Some text in the modal.</p>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n      </div>\n    </div>\n\n  </div>\n  "
        })
    ], DucksEditWindowComponent);
    return DucksEditWindowComponent;
}());
exports.DucksEditWindowComponent = DucksEditWindowComponent;
//# sourceMappingURL=ducks-edit.component.js.map