import { Component, Input, Output } from '@angular/core';

import { AdSpaceService } from '../../../services/adspace.service';

@Component({
  moduleId: module.id,
  selector: 'ducks-edit',
  templateUrl: 'ducks-edit.component.html',
  styleUrls: ['ducks-edit.component.css']
})
export class DucksEditComponent {

  coords;

  constructor(
    private adSpaceService: AdSpaceService,
  ){}

  ngOnInit() {
    this.loadAllBBLocation();
    this.coords = [];
  }

  loadAllBBLocation() {
    this.adSpaceService.getAllBillboards()
      .subscribe(loc => {
        var z = loc;
        delete z["username"];
        var x = Object.keys(z);
        x.forEach(y => {
          this.coords.push({lat: parseFloat(z[y].lat), lng: parseFloat(z[y].lng)});
        })
      });
  }

  openEditWindow(o) {
    console.log(o);
  }

  logpappu() {
    console.log("pappu");
  }
}

@Component({
  moduleId: module.id,
  selector: 'ducks-edit-window',
  template: `
  <div class="modal fade">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
  `
})

export class DucksEditWindowComponent {

}
