"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var adspace_service_1 = require("../../../services/adspace.service");
var router_1 = require("@angular/router");
var DucksAddComponent = (function () {
    function DucksAddComponent(adSpaceService, router) {
        this.adSpaceService = adSpaceService;
        this.router = router;
    }
    DucksAddComponent.prototype.ngOnInit = function () {
        this.place = "Test place";
        this.lat = 9.978034;
        this.lng = 76.277107;
        this.price = 15000;
        this.size = "4x8";
    };
    DucksAddComponent.prototype.logData = function () {
        console.log(this.lat, this.lng, this.price, this.size);
    };
    DucksAddComponent.prototype.addNewBB = function () {
        var _this = this;
        this.adSpaceService.addBillboard({ place: this.place, lat: this.lat, lng: this.lng, price: this.price, size: this.size })
            .subscribe(function (res) {
            _this.router.navigate(['/agency', 'edit']);
        });
    };
    DucksAddComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'ducks-add',
            templateUrl: 'ducks-add.component.html',
            styleUrls: ['ducks-add.component.css']
        }),
        __metadata("design:paramtypes", [adspace_service_1.AdSpaceService,
            router_1.Router])
    ], DucksAddComponent);
    return DucksAddComponent;
}());
exports.DucksAddComponent = DucksAddComponent;
//# sourceMappingURL=ducks-add.component.js.map