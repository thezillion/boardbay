import { Component, OnInit } from '@angular/core';

import { AdSpaceService } from '../../../services/adspace.service';

import { Router } from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'ducks-add',
  templateUrl: 'ducks-add.component.html',
  styleUrls: ['ducks-add.component.css']
})

export class DucksAddComponent {
    place: string;
    lat: number;
    lng: number;
    price: number;
    size:string;

    constructor(
      private adSpaceService: AdSpaceService,
      private router: Router
    ){

    }

    ngOnInit() {
      this.place = "Test place";
      this.lat = 9.978034;
      this.lng = 76.277107;
      this.price = 15000;
      this.size = "4x8";
    }

    logData(): void {
      console.log(this.lat, this.lng, this.price, this.size);
    }

    addNewBB(): void {
      this.adSpaceService.addBillboard({ place: this.place, lat: this.lat, lng: this.lng, price: this.price, size: this.size })
        .subscribe(res => {
          this.router.navigate(['/agency', 'edit']);
        });
    }
}
